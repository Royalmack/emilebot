﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Discord;
using System.Text.RegularExpressions;
using System.Configuration;

namespace ToGBot
{
    public class ToGBot
    {
        private DiscordClient _client;

        public ToGBot()
        {
            //Set up discord client
            _client = new DiscordClient();

            //connect to Database
           

            _client.ExecuteAndWait(async() =>
            {
                _client.MessageReceived += _client_MessageReceived;

                _client.UserJoined += _client_UserJoined;

                _client.Ready += _client_Ready;

                await _client.Connect(ConfigurationManager.AppSettings["token"]);
            });
        }

        private void _client_UserJoined(object sender, UserEventArgs e)
        {
            e.Server.DefaultChannel.SendMessage("Huh Ho~ It's been a while since there's been a visitor like you here " + e.User.Mention);
        }

        private void _client_Ready(object sender, EventArgs e)
        {
            _client.SetGame("!help");
            return;
        }

        private void _client_MessageReceived(object sender, MessageEventArgs e)
        {            

            if (e.Message.IsAuthor)
                return;
            else
            {
                Console.WriteLine("{0} said: {1}", e.Message.User.Name, e.Message.Text);
            }

            string userMessage = e.Message.Text;

            string[] words = userMessage.Split(' ');

            
            if (words[0] == "!help")
            {
                e.Channel.SendMessage("Bot in construction. Please be patient. \n!team - list teams to join \n!release ### - Get linked to a chapter of choice");
                return;
            }

            if (words[0] == "!team")
            { 
                if (words.Count() < 2)
                {
                    var server_Roles = e.Server.Roles;
                    List<Role> roleList = new List<Role>();

                    String teamString = "";

                    foreach(Role sRole in server_Roles)
                    {
                        if(sRole.Name.StartsWith("Team"))
                        {
                            teamString = teamString + "!" + sRole.Name.ToString().ToLower() + "\n";
                        }
                    }

                    e.Channel.SendMessage(teamString);

                    return;
                }
                else
                {
                    bool check = false;

                    //create role check to determine team
                    string roleCheck;
                    if (words.Count() == 2)
                    {
                         roleCheck = "Team " + words[1];
                    }
                    else
                    {
                         roleCheck = "Team " + words[1] + " " + words[2];
                    }

                    //get current users roles
                    var userRoles = e.User.Roles;

                    //Create with desired roles
                    List<Role> finalRoles = new List<Role>();

                    //Get all server roles and create a list of roles beginning with Team
                    var serverRoles = e.Server.Roles;
                    List<Role> teamRoles = new List<Role>();

                    foreach (Role r in serverRoles)
                    {
                        if (r.Name.StartsWith("Team"))
                        {
                            teamRoles.Add(r);
                        }
                    }

                    //Check each role of team members to see if desired team exists
                    foreach (Role r in teamRoles)
                    {
                        if (r.Name.ToString().ToLower() == roleCheck.ToLower()) 
                        {  
                            finalRoles.Add(r);
                            check = true;
                            words[1] = "";
                        }                       
                    }

                    //Remove any team roles from current user while keeping others
                    if (check)
                    {
                        foreach (Discord.Role r in userRoles)
                        {
                            if (!r.Name.StartsWith("Team"))
                            {
                                finalRoles.Add(r);
                            }
                        }

                        //Edit user using finalRoles
                        e.User.Edit(roles: finalRoles);
                        reply(e, " joined " + roleCheck);

                        return;
                    }
                    else
                    {
                        e.Channel.SendMessage("Role does not exist");
                        return;
                    }                        
                }
            }

            if (words[0] == "!bae")
            {
                foreach (var role in e.User.Roles)
                {
                    if (role.Name.StartsWith("Team"))
                    {
                        var fileName = Regex.Replace(role.Name.Substring(4), @"\s+", "").ToLower();
                        Console.WriteLine(fileName);
                        if (File.Exists("images/" + fileName + ".png"))
                        {
                            e.Channel.SendFile("images/" + fileName + ".png");
                            return;
                        }
                        else if (File.Exists("images/" + fileName + ".jpg"))
                        {
                            e.Channel.SendFile("images/" + fileName + ".jpg");
                            return;
                        }
                    }
                    e.Channel.SendMessage("Bae not good enough!");
                    return;
                }
            }
            
            //Used to link to specific releases of Tower of God
            if (words[0] == "!release")
            {
                try
                {
                    int id = Int32.Parse(words[1]);
                    e.Channel.SendMessage("*Chapter ID:* " + id + "\n*Link:* http://www.webtoons.com/en/fantasy/tower-of-god/s/viewer?title_no=95&episode_no=" + id);
                } catch (SystemException err)
                {
                    e.Channel.SendMessage("Incorrect ID!");
                }
            }

            if(words[0] == "!active")
            {
                try
                {
                    var admin = e.Server.FindRoles("Irregulars").FirstOrDefault();
                    var channel = e.Server.FindChannels("general").FirstOrDefault();

                    if (e.User.HasRole(admin))
                    {
                        //gather list of server users and cycle through them
                        var _users = channel.Users;
                        foreach(var user in _users)
                        {
                            e.Channel.SendMessage(user.Name + ": " + channel.GetUser(user.Id).LastActivityAt.ToString() + "\n");
                        }

                        return;
                    }
                    else
                    {
                        e.Channel.SendMessage("Invalid permissions.  Role of " + admin + " needed");
                        return;
                    }
                }
                catch(SystemException err)
                {
                    e.Channel.SendMessage(err.Message);
                }
            }
        }
        public void reply(MessageEventArgs e, String message)
        {
            e.Channel.SendMessage(e.User.Mention + message);
        }
        static void Main(string[] args)
        {
            ToGBot bot = new ToGBot();
        }
    }

}
